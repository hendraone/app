FROM node:12
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
COPY package*.json yarn.lock ./
RUN yarn
COPY . ./app
EXPOSE 3000
CMD [ "node", "index.js" ]
